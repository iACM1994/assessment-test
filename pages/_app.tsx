import '../styles/globals.css'
import type { AppProps } from 'next/app'
import MainLayout from '../components/Layouts'
import Head from 'next/head'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <MainLayout>
      {/* <Head>
        <link
          rel="preload"
          href="/fonts/GraphikRegular.otf"
          as="font"
          type="font/otf"
          crossOrigin=""
        />
        <link
          rel="preload"
          href="/fonts/GraphikMedium.otf"
          as="font"
          type="font/otf"
          crossOrigin=""
        />
      </Head> */}
      <Component {...pageProps} />
    </MainLayout>
  )
}

export default MyApp
