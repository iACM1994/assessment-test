import type { NextPage } from 'next';
import Head from 'next/head';
import { useEffect, useState } from 'react';
import Article from '../components/Article';
import articleData from '../util/data/article-316751.json';
import popularData from '../util/data/sideBar_popular.json';

const Home: NextPage = () => {

  const [article, setArticle] = useState<any>(null);
  const [popular, setPopular] = useState<any>([]);
  const [pageTitle, setPageTitle] = useState<string>("Today");
  useEffect(() => {
    articleData && setArticle(articleData[0]);
    popularData && setPopular(popularData.items.map((item:any) => ({
      id: item.id,
      image: item.image,
      title: item.title,
      url: item.url,
    })));
  }, []);
  
  useEffect(() => {
    article && setPageTitle(article.title);
  }, [article])

  return (
    <div>
      <Head>
        <title>{pageTitle}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Article article={article} popular={popular} />
    </div>
  )
}

export default Home
