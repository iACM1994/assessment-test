import Link from 'next/link';
import React, { useState } from 'react';
import styles from './NavItems.module.css';

const NavItems = ({ items, stick }:any) => {
  const [mobileNavOpen, setMobileNavOpen] = useState<boolean>(false);

  const navToggle = () => {
    setMobileNavOpen(!mobileNavOpen);
  }

  return (
    <div className={`${styles.navItems} ${stick ? styles.sticky : ''}`}>
      <div className={styles.mobileIcon}>
        <div className={styles.mobileIconWrapper}>
          <button
            className={`${styles.mobileNavBtn} ${mobileNavOpen ? styles.open : styles.close}`}
            onClick={() => navToggle()}
          >
            Menu 
          </button>
        </div>
      </div>
      <nav>
        <ul className={`${mobileNavOpen ? styles.menuOpen : ''}`}>
          {
            items.map((item:any) => <li key={item.uri} className={styles.navItem}>
              <Link href={item.absolute_url}>
                <a>
                  {item.title}
                </a>
              </Link>
            </li>)
          }
        </ul>
      </nav>
    </div>
  )
}

export default NavItems;