import React, { useEffect, useRef, useState } from 'react';
import Image from 'next/image'
import Link from 'next/link';
import styles from './Navigation.module.css'

import navData from '../../../util/data/header.json'
import NavItems from './NavItems';

const Navigation = () => {
  const [stick, setStick] = useState<boolean>(false);

  const checkScroll = () => {
    const navBlock = document.getElementById('navigation');
    const windowPageOffset = window.pageYOffset;
    const navPosition = navBlock?.offsetTop || 60;
    if (windowPageOffset > navPosition + 60) {
      setStick(true);
    } else {
      setStick(false);
    }
  }

  useEffect(() => {
    checkScroll();
    document.addEventListener('scroll', checkScroll);

    return () => document.removeEventListener('scroll', checkScroll);
  }, []);

  return (
    <div className={`${styles.navContainer}  ${stick && `${styles.sticky}`}`} id="navigation">
      <div className={styles.navBody}>
        <div className={styles.logo}>
          <div className={styles.logoImg}>
            <Link href="/">
              <a>
                <Image src="/logo.svg" alt="today" width={760} height={240} />
              </a>
            </Link>
          </div>
        </div>
        <NavItems items={navData} sticked={stick} />
        <div className={styles.navProfile}>
          <div className={styles.profileContainer}>
            <Link href="/">
              <a className={styles.profile}>
                <Image src="/profile.svg" alt="user" width={16} height={16} />
                <span className={styles.profileText}>Sign In</span>
              </a>
            </Link>
            <Link href="/">
              <a className={styles.profile}>
                <Image src="/search.svg" alt="search" width={16} height={16} />
                <span className={styles.profileText}>Search</span>
              </a>
            </Link>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Navigation;
