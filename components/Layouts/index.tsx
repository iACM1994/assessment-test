import React, { ReactNode } from 'react';
import Navigation from './Navigation';

const MainLayout = (props:any) => {
  return (
    <div className="main">
      <div className="container">
        <div className="header">
          <Navigation />
        </div>
        <div className="content">
          <main>
            {props.children}
          </main>
        </div>
      </div>
    </div>
  )
}

export default MainLayout;
