import Link from 'next/link';
import React from 'react';
import styles from './ReadMore.module.css';

const ReadMore = () => {

  return (
    <div className={styles.readMoreContainer}>
      <p className={styles.description}>
        Read more of the latest in&nbsp;
        <span className={styles.singapore}>Singapore</span>
      </p>
      <span className={styles.explore}>
        <Link href="/">
          <a>Explore Now</a>
        </Link>
      </span>
    </div>
  )
}

export default ReadMore;
