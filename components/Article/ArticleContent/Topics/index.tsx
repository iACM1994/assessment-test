import Link from 'next/link';
import React from 'react';
import styles from './Topics.module.css';

const Topics = (props:any) =>  {
  const { data } = props;
  return (
    <div className={styles.topicsContainer}>
      <h2 className={styles.topicsTitle}>
        Related Topics
      </h2>
      <div className={styles.topicsWrapper}>
        {
          data.map((topic:any) => (
            <div className={styles.topic} key={topic.id}>
              <Link href="/">
                <a>{topic.name}</a>
              </Link>
            </div>
          ))
        }
      </div>
    </div>
  )
}

export default Topics;
