import React, { ChangeEvent, useState } from 'react';
import Image from 'next/image';
import styles from './Newsletter.module.css';

const Newsletter = (props:any) => {
  const { data } = props;
  const [email, setEmail] = useState<string>('');

  return (
    <div className={styles.newsletterContainer}>
      <div className={styles.nlTitle}>
        <h2>{data.title}</h2>
      </div>
      <div className={styles.nlBodyWrapper}>
        <div className={styles.nlBody}>
          <div className={styles.nlBodyText}>
            <div dangerouslySetInnerHTML={{ __html: data.body }} />
          </div>
          <div className={styles.nlImg}>
            <picture>
              <img src={data.newsletter_image} alt={data.label} />
            </picture>
          </div>
        </div>
        <div className={styles.nlForm}>
          <form>
            <input
              type="text"
              className={styles.nlEmailInput}
              name="subscription_email"
              value={email}
              placeholder={data.placeholder}
              onChange={(e:any) => setEmail(e.target.value)}
            />
            <button
              type="submit"
              className={styles.nlSubscribeBtn}
            >
              Subscribe
            </button>
          </form>
        </div>
        <div className={styles.nlDescription}>
          <p>{data.sub_description}</p>
        </div>
      </div>
    </div>
  )
}

export default Newsletter;