import Link from 'next/link';
import React from 'react';
import styles from './PopularItem.module.css';

const PopularItem = (props:any) => {
  const { data } = props;

  return (
    <div className={styles.popularItem}>
      <div className={styles.popularItemBox}>
        <div className={styles.popularImg}>
          <picture>
            <img src={data.image} alt={data.id} />
          </picture>
        </div>
        <div className={styles.popularTitle}>
          <p>{data.title}</p>
        </div>
        <Link href="/">
          <a className={styles.popularLink} />
        </Link>
      </div>
    </div>
  )
}

export default PopularItem;
