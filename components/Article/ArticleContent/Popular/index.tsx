import React from 'react';
import styles from './Popular.module.css';
import PopularItem from './PopularItem';

const Popular = (props:any) => {
  const { data } = props;

  return (
    <div className={styles.popularContainer}>
      <div className={styles.popularBox}>
        <div className={styles.popularHeader}>
          <h2>Popular</h2>
        </div>
        <div className={styles.popularWrapper}>
          {
            data.map((item:any) => (<div className={styles.popularItem} key={item.id}>
                <PopularItem data={item} />
              </div>)
            )
          }
        </div>
      </div>
    </div>
  )
}

export default Popular;
