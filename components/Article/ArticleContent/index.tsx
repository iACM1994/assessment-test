import React, { useEffect, useState } from 'react';
import Topics from './Topics';
import styles from './ArticleContent.module.css';
import ReadMore from './ReadMore';
import Newsletter from './Newsletter';
import Spotlight from './Spotlight';
import Popular from './Popular';
import ReadAlso from './ReadAlso';

const ArticleContent = (props:any) => {
  const { data } = props;
  const { readAlso } = data;
  const [contents, setContents] = useState<any>(null);
  const [articleBody, setArticleBody] = useState<any>();
  const [updated, setUpdated] = useState<boolean>(false);

  useEffect(() => {
    const contents = document.getElementById('articleContent') as HTMLObjectElement;
    setContents(contents?.childNodes);
  }, []);

  useEffect(() => {
    if (readAlso.length > 0 && contents && !updated) {
      const contentArray = Object.values(contents);
      const components:Array<any> = [];
      const divider = Math.floor(contentArray.length / readAlso.length);
      contentArray.map((content:any, index:number) => {
        if ((index + 1) % divider === 0 && ((index + 1)/divider) - 1 <= readAlso.length) {
          const component = <ReadAlso data={readAlso[((index + 1)/divider) - 1]} key={index}>
              <div dangerouslySetInnerHTML={{ __html: content.outerHTML }} />
            </ReadAlso>
          components.push(component);
        } else {
          const component = <div key={index} dangerouslySetInnerHTML={{ __html: content.outerHTML }} />
          components.push(component);
        }
      });
      setArticleBody(components);
      setUpdated(true);
    }
  }, [contents, readAlso, updated]);

  return (
    <div className={styles.articleContentBox}>
      <div className={styles.left}>
        <div className={styles.articleContentWrapper}>
          {
            !updated ?
            <div id="articleContent" dangerouslySetInnerHTML={{ __html: data.content.body }} />
            :
            <div>
              {
                articleBody.map((art:any) => art)
              }
            </div>
          }
        </div>
        { data.topics && <Topics data={data.topics} /> }
        <ReadMore />
        <div className={styles.contentExtras}>
          <Newsletter data={data.newsletter} />
          <Spotlight data={data.spotlight} />
        </div>
      </div>
      <div className={styles.right}>
        <Popular data={data.popular} />
      </div>
    </div>
  )
}

export default ArticleContent;