import Link from 'next/link';
import React from 'react';
import styles from './ReadAlso.module.css';

const ReadAlso = (props:any) => {
  const { data, children } = props;

  return (
    <div className={styles.readAlsoComponent}>
      <div className={styles.content}>
        {children}
      </div>
      <div className={styles.readAlsoItem}>
        <div className={styles.readAlsoWrapper}>
          <h4>{data.label}</h4>
          <Link href={data.link}>
            <a>{data.title}</a>
          </Link>
        </div>
      </div>
    </div>
  )
}

export default ReadAlso;
