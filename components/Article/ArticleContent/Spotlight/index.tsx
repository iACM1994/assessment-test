import React from 'react';
import Link from 'next/link';
import styles from './Spotlight.module.css';

const Spotlight = (props:any) => {
  const { data } = props;

  return (
    <div className={styles.spotlightContainer}>
      <div className={styles.spotlightImg}>
        <Link href={data.view_more.url}>
          <a>
            <picture>
              <img src={data.hero_media.media_image} alt={data.title} />
            </picture>
          </a>
        </Link>
      </div>
    </div>
  )
}

export default Spotlight;
