import React, { useState, useEffect } from 'react'
import styles from './Article.module.css'
import ArticleContent from './ArticleContent';
import ArticleHead from './ArticleHead';
import Topics from './ArticleContent/Topics';

const Article = (props:any) => {
  const { article, popular }:any = props
  const [header, setHeader] = useState<any>();
  const [content, setContent] = useState<any>();

  useEffect(() => {
    if (article) {
      const headerData = {
        title: article.title,
        image: {
          url: article.image.media_image,
          bylineTag: article.image.image_byline_and_source.byline,
          description: article.image.description,
          alt: article.image.name
        },
        byline_detail: {
          authors: article.byline_detail.map((item:any) => ({
            name: item.title,
            link: item.url,
            image: item.hero_media.thumbnail,
          })),
          publishDate: article.publishdate,
          lastUpdated: article.lastupdated,
        }
      }
      setHeader(headerData);

      const contentData = {
        content: article.content.filter((data:any) => data.bundle === 'text')[0],
        topics: article.topics,
        newsletter: article.content.filter((data:any) => data.bundle === 'newsletter_subscription')[0],
        spotlight: article.content.filter((data:any) => data.bundle === 'spotlight')[0],
        popular,
        readAlso: article.components,
      }
      setContent(contentData);
    }
  }, [article, popular]);

  return (
    <div className={styles.articleContainer}>
      { header && <ArticleHead data={header} /> }
      { content && <ArticleContent data={content} /> }
    </div>
  )
}

export default Article;
