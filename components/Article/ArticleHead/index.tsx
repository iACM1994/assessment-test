import React from 'react';
import Link from 'next/link';
import styles from './ArticleHead.module.css';
import moment from 'moment';

const ArticleHead = (props:any) => {
  const { data } = props;

  const mediaLinks = [
    { name: 'bookmark', link: '/' },
    { name: 'whatsup', link: '/' },
    { name: 'telegram', link: '/' },
    { name: 'facebook', link: '/' },
    { name: 'twitter', link: '/' },
    { name: 'mail', link: '/' },
    { name: 'linkedin', link: '/' },
  ]

  return (
    <div className={styles.articleHeadContainer}>
      <div className={styles.title}>
        <p>{data.title}</p>
      </div>
      <div className={styles.articleHeadBox}>
        <div className={styles.left}>
          <div className={styles.image}>
            <div className={styles.imgContainer}>
              <picture>
                <img src={data.image.url} alt={data.image.alt} width={762} height={572} />
              </picture>
            </div>
            <div className={styles.bylineTags}>
              <span>{data.image.bylineTag.join(', ')}</span>
            </div>
          </div>
          <div className={styles.imgDescription}>
            <div dangerouslySetInnerHTML={{ __html: data.image.description }} />
          </div>
          <div className={styles.socialBanner}>
            <p>
              Follow us on&nbsp;
              <Link href="/">
                <a>Instagram</a>
              </Link> 
              &nbsp;and&nbsp;
              <Link href="/">
                <a>Tiktok</a>
              </Link>
              , and join our&nbsp;
              <Link href="/">
                <a>Telegram</a>  
              </Link>&nbsp;channel for the latest updates.
            </p>
          </div>
        </div>
        <div className={styles.right}>
          <div className={styles.bylineAuthors}>
            {
              data.byline_detail.authors.map((author:any) => {
                return (
                  <div className={styles.authorLine} key={author.name}>
                    <div className={styles.authorImg}>
                      <Link href={author.link}>
                        <a>
                          <picture>
                            <img src={author.image} alt={author.name} width={50} height={50} />
                          </picture>
                        </a>
                      </Link>
                    </div>
                    <div className={styles.authorName}>
                      <p><i>BY</i>&nbsp;&nbsp;<b>{author.name}</b></p>
                    </div>
                  </div>
                );
              })
            }
            <div className={styles.dates}>
              <div className={styles.date}>
                <p><i>Published</i> <b>{moment(data.byline_detail.publishDate).format('MMMM DD, YYYY')}</b></p>
              </div>
              <div className={styles.date}>
                <p><i>Updated</i> <b>{moment(data.byline_detail.lastUpdated).format('MMMM DD, YYYY')}</b></p>
              </div>
            </div>
            <div className={styles.medias}>
              {
                mediaLinks.map((media:any) => (
                  <Link href={media.link} key={media.name}>
                    <a>
                      <picture>
                        <img src={`/social-icons/${media.name}-icon.png`} alt={media.name} width={30} height={30} />
                      </picture>
                    </a>
                  </Link>
                ))
              }
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ArticleHead;